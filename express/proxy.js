const express = require('express');
const axios = require('axios')

// Initialize express engine
const app = express();

app.all('*', async (req, res, next) => {

  // Allow connections from everywhere
  res.header("Access-Control-Allow-Origin", "*");

  // Limit type of calls (this app only uses GET)
  res.header("Access-Control-Allow-Methods", "GET");

  // Pass through headers, includes Bearer Token
  res.header("Access-Control-Allow-Headers", req.header('access-control-request-headers'));

  // If method is OPTIONS, send an empty response (hide cors)
  if (req.method === 'OPTIONS') {
    res.send();
  } else {

    // The destination URL is the final location of the API call (past the proxy)
    const destinationURL = req.header('Destination');

    // If destinationURL is unset, return error message
    if (!destinationURL) {
      res.status(403).send({ error: 'Please specify a Destination URL' });
      return;
    }

    // Bearer token is passed in as header to allow alternative uses of this proxy
    const bearerToken = req.header('BearerToken');
    if (!bearerToken) {
      res.status(403).send({ error: 'Please specify a Bearer Token' });
      return;
    }

    let response;
    try {
      response = await axios
        .get(destinationURL, bearerToken ? {headers: {Authorization: bearerToken}} : {})
    } catch(e) {
      console.log("Error connecting to API via proxy", e);
      res.status(400).send({ error: `Error connecting to API via proxy` });
    }

    // If response exists, send it to the requesting node
    if(response)
      res.status(200).send(response.data)
  }
});

// Set Listening port
app.set('port', process.env.PORT || 5000);

// Start listening
app.listen(app.get('port'), () => {
  console.log('Proxy listening on port ' + app.get('port'));
});