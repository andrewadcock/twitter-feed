# Twitter Feed

## Author Information
**Name**: Andrew Adcock

**Email**: andrewadcock@gmail.com

**Website**: [https://andrewadcock.com](https://andrewadcock.com)

## Usage Instructions
Clone the repository

`git clone https://gitlab.com/andrewadcock/twitter-feed`

CD in to the directory

`cd twitter-feed`

Add .env file in ${ROOT}/react. Copy .env-example to .env and modify as needed.

`cd react && cp .env-example .env`

Return to ${ROOT} and start containers

`cd ../ && docker-compose up`

View site in browser at [http://localhost:3000](http://localhost:3000)

#### Optional:
Run yarn install inside react for IDE completion

`cd ./react && yarn install`


Getting an error? Different OS's handle mounting differently. Check the directory owner and reset if necessary. Here is 
an example (make sure to replace user:user with your user and group):
`sudo chown user:user -R node_modules`


## Run Tests
Navigate to the ${ROOT}/react and run

`yarn test`


## Missing Functionality
One of the requirements for this project was to append a clickable link to the text of each tweet. This presents a 
problem because not all tweets have a URL returned. It appears there was a change to the Twitter API that truncates 
tweet text and appends a URL directly to the text body. This unfortunately happens regardless of using 
`tweet_mode=extended`. When a URL is available, it is automatically appended to the tweet text and available in the data
object. If the URL is not available in the data object, it is also not appended to the tweet. This means, there is no
reliable way to show a url at the end of each tweet.

Max_ID also appears to be handled differently. In my testing max_id did not contain a value until after the last call 
was made. In addition, the `next_results` response does not always contain more data. As a result, the last user call 
may return empty. 


