import axios from "axios";

const retrieveTweetsByKeyword = async (
  keyword: string,
  nextResults: string | null = null
) => {
  const proxyURI = process.env.REACT_APP_PROXY_URI ?? "http://127.0.0.1:5000";
  const bearerToken = process.env.REACT_APP_BEARER_TOKEN;

  const baseTarget = "https://api.twitter.com/1.1/search/tweets.json";
  const defaultParams = `?q=${keyword}&result_type=popular&count=5&tweet_mode=extended`;

  // Set target to passed in params if available
  const target = `${baseTarget}${nextResults ?? defaultParams}`;

  let data;
  try {
    data = await axios.get(proxyURI, {
      headers: { Destination: target, BearerToken: `Bearer ${bearerToken}` },
    });
  } catch (e) {
    console.log("Error retrieving data from API", e);
    return {
      error: "Error retrieving data from API",
    };
  }

  if (data && data.data) {
    return data.data;
  }
};

export { retrieveTweetsByKeyword };
