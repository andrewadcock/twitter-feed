import React from "react";
import Tweets from "./components/tweets/Tweets";
import "./App.scss";

function App() {
  return (
    <div className="App">
      <Tweets />
    </div>
  );
}

export default App;
