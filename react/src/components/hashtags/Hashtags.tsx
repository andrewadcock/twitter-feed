import IHashtagsComponent from "../../interfaces/HashtagsComponent";

const Hashtags = ({
  data,
  setSelectedHashtag,
  selectedHashtag,
}: IHashtagsComponent) => {
  // Generate hashtag list
  const hashtagList = () => {
    if (!data.length)
      return <div className="empty-results">No hashtags available</div>;

    return data.map((tag: string, index: number) => (
      <button
        onClick={(e) => setSelectedHashtag(tag ?? "")}
        className={selectedHashtag === tag ? "active" : ""}
        key={index}
      >
        {" "}
        #{tag}
      </button>
    ));
  };
  return (
    <div className="hashtags">
      <h2>Filter by hashtag</h2>
      <div className="tag-container">{hashtagList()}</div>
    </div>
  );
};

export default Hashtags;
