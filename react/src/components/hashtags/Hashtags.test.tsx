import { mount } from "enzyme";
import Hashtags from "./Hashtags";

describe("Renders Hashtags", () => {
  const data = ["dogs", "cat", "BelgianMalinois"];

  const setSelectedHashtagMock = jest.fn();

  let wrapper = mount(
    <Hashtags data={data} setSelectedHashtag={setSelectedHashtagMock} key={1} />
  );

  test("Check number of hastags", () => {
    expect(wrapper.find("button")).toHaveLength(3);
  });

  test("Check clicking button", () => {
    wrapper.find("button").first().simulate("click");
    expect(setSelectedHashtagMock).toHaveBeenCalledTimes(1);
    expect(setSelectedHashtagMock).toHaveBeenCalledWith("dogs");
  });

  test("Check if empty results message does NOT displays", () => {
    expect(wrapper.find(".empty-results")).toHaveLength(0);
  });

  // Begin emtpy data set
  let data2 = [];
  let wrapperEmpty = mount(
    <Hashtags
      data={data2}
      setSelectedHashtag={setSelectedHashtagMock}
      key={1}
    />
  );
  test("Check if empty results message displays", () => {
    expect(wrapperEmpty.find(".empty-results")).toHaveLength(1);
  });
});
