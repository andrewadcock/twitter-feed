import { shallow, mount } from "enzyme";
import Tweets from "./Tweets";
import Hashtags from "../hashtags/Hashtags";

describe("Renders Tweet component", () => {
  let wrapper = shallow(<Tweets />);

  test("Ensure Header exists", () => {
    expect(wrapper.find("h1")).toHaveLength(1);
    expect(wrapper.find("h1").first().text()).toBe("Twitter Feed");

    expect(wrapper.find(".search-box")).toHaveLength(1);
    expect(wrapper.find("input")).toHaveLength(1);

    expect(wrapper.find(".search-button")).toHaveLength(1);
    expect(wrapper.find(".search-button").first().text()).toBe(
      "<FontAwesomeIcon />"
    );
  });

  test("Ensure Hashtags component renders", () => {
    expect(wrapper.find(Hashtags)).toHaveLength(1);
  });

  test("Ensure Initial render show empty results message", () => {
    expect(wrapper.find(".empty-results")).toHaveLength(1);
    expect(wrapper.find(".empty-results").first().text()).toBe(
      "Please enter a keyword and Search"
    );
  });
});
