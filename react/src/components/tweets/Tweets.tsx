import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { useDebounce } from "tiny-use-debounce";
import { retrieveTweetsByKeyword } from "../../helpers/api";
import Tweet from "../tweet/Tweet";
import Hashtags from "../hashtags/Hashtags";
import ITweets from "../../interfaces/Tweets";
import ITweet from "../../interfaces/Tweet";
import IHashtag from "../../interfaces/Hashtag";

const Tweets = () => {
  const initialTweetsState = {
    search_metadata: {},
    statuses: [],
  };

  const DEBOUNCE_WAIT = 500;
  const [keyword, setKeyword] = useState<string>("");
  const [tweets, setTweets] = useState<ITweets>(initialTweetsState);
  const [hashtags, setHashtags] = useState<string[]>([]);
  const [selectedHashtag, setSelectedHashTag] = useState<string>("");
  const [maxID, setMaxID] = useState<number>();
  const [searchInitiated, setSearchInitiated] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  // Helper function that can be passed to children
  const updateHashtag = (tag: string) => {
    setSelectedHashTag(selectedHashtag !== tag ? tag : "");
  };

  // Construct tweets from tweets storage var
  const buildTweets = () => {
    // If no tweets, display error message
    if (!tweets.statuses.length) {
      return (
        <div className="empty-results">
          {searchInitiated ? <strong>No Results. </strong> : ""}Please enter a
          keyword and Search
        </div>
      );
    }

    let finalStatuses = tweets.statuses;

    // If hashtag is selected, filter results for tweets with matching tag and set to finalStatuses
    if (selectedHashtag) {
      // Filter Statuses containing the selectedHashtag
      finalStatuses = tweets.statuses.filter((status: ITweet) => {
        // If any hashtags match the currently selected hashtag there will be a length
        return status.entities.hashtags.filter((tag: IHashtag) => {
          return tag.text === selectedHashtag;
        }).length;
      });
    }

    return finalStatuses.map((tweet: ITweet, index: number) => (
      <Tweet
        data={tweet}
        setSelectedHashtag={updateHashtag}
        selectedHashtag={selectedHashtag}
        key={index}
      />
    ));
  };

  // Retrieve Tweets
  const handleSearch = useDebounce(async (e: React.SyntheticEvent) => {
    e.preventDefault(); // prevent page reload
    setSearchInitiated(true);
    setLoading(true);
    if (!keyword) return;

    const data = await retrieveTweetsByKeyword(keyword);

    // If data, set it to state, and set max_id if available (this feature appears to be deprecated)
    if (data.statuses.length) {
      setTweets(data);
      setMaxID(data.search_metadata.max_id);
    } else {
      setTweets(initialTweetsState);
      setHashtags([] as string[]);
    }

    // Clear hashtags on every search
    setSelectedHashTag("");
    setLoading(false);
    // If no data set maxID to a truthy
    if (!data.statuses.length) setMaxID(1);
  }, DEBOUNCE_WAIT);

  // Add more tweets
  const loadMore = useDebounce(async () => {
    // Clear hashtag to prevent an empty-appearing load process
    setSelectedHashTag("");
    setLoading(true);

    const tempTweets = { ...tweets };
    const nextResults = tempTweets.search_metadata.next_results;

    const data = await retrieveTweetsByKeyword(keyword, nextResults);

    // If data is empty, trigger maxID to truth
    if (!data.statuses.length) setMaxID(1);

    tempTweets.statuses = tempTweets?.statuses?.concat(data.statuses);
    tempTweets.search_metadata.next_results = data.search_metadata.next_results;
    setLoading(false);
    if (data.statuses.length) setTweets(tempTweets);
  }, DEBOUNCE_WAIT);

  // When tweets are updated, update available hashtags as well
  useEffect(() => {
    if (!tweets) {
      return;
    }

    // Loop through all statuses and return array of hashtags
    const availableHashtags = tweets.statuses
      .map((status: ITweet) => {
        return status.entities.hashtags.map((single: IHashtag) => single.text);
      })
      .flat();

    // Remove duplicates and set to state (create a set, then create array from set)
    setHashtags([...Array.from(new Set(availableHashtags))] as string[]);
  }, [tweets]);

  return (
    <div className="tweets-container">
      <h1>
        <a href="/">Twitter Feed</a>
      </h1>
      <div className="tweets-grid">
        <div className="search-box">
          <form
            onSubmit={(e) => {
              e.preventDefault();
              handleSearch(e);
            }}
            data-testid="form-search"
          >
            <div className="search-container">
              <button type="submit" className="search-button">
                <FontAwesomeIcon icon={faSearch} />
              </button>
              <input
                type="text"
                value={keyword}
                onChange={(e: React.FormEvent<HTMLInputElement>) =>
                  setKeyword(e.currentTarget.value)
                }
                placeholder="Search by keyword"
              />
            </div>
          </form>
        </div>
        <div>
          <Hashtags
            data={hashtags}
            setSelectedHashtag={updateHashtag}
            selectedHashtag={selectedHashtag}
          />
        </div>
        <div className="tweets-wrapper">
          {buildTweets()}

          {tweets && tweets.statuses.length && !maxID ? (
            <footer>
              <button onClick={loadMore}>Load more</button>
            </footer>
          ) : null}
          {tweets && tweets.statuses.length && maxID ? (
            <div className="empty-results">End of results</div>
          ) : null}
        </div>
      </div>
      {loading ? (
        <div className="loader-container">
          <div className="loader"></div>
          <div>Loading...</div>
        </div>
      ) : null}
    </div>
  );
};

export default Tweets;
