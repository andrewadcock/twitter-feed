import IHashtag from "../../interfaces/Hashtag";
import ITweetComponent from "../../interfaces/TweetComponent";

const Tweet = ({
  data,
  setSelectedHashtag,
  selectedHashtag,
}: ITweetComponent) => {
  // Build hashtags
  const hashtags = () => {
    return data.entities.hashtags.map((hashtag: IHashtag, index: number) => {
      return (
        <button
          onClick={() => setSelectedHashtag(hashtag.text ?? "")}
          key={index}
          className={selectedHashtag === hashtag.text ? "active" : ""}
        >
          #{hashtag.text}
        </button>
      );
    });
  };
  return (
    <div className="tweet">
      <div className="avatar-container">
        <a
          href={`https://twitter.com/${data.user.screen_name}`}
          target="_blank"
          rel="noreferrer"
        >
          <img
            className="avatar"
            src={data.user.profile_image_url}
            alt={data.user.screen_name}
          />
        </a>
      </div>
      <div className="tweet-content">
        <div className="screen-name">
          <a
            href={`https://twitter.com/${data.user.screen_name}`}
            target="_blank"
            rel="noreferrer"
          >
            @{data.user.screen_name}
          </a>
        </div>
        <div data-testid="tweet-text">{data.full_text}</div>

        <div className="tags">{hashtags()}</div>
      </div>
    </div>
  );
};
export default Tweet;
