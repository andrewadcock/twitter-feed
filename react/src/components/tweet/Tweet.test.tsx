import { shallow } from "enzyme";
import Tweet from "./Tweet";
import tweetMock from "../../mocks/tweetMock";

describe("Renders Tweet component", () => {
  const selectedHashtag = jest.fn();

  let wrapper = shallow(
    <Tweet data={tweetMock} selectedHashtag={selectedHashtag} key={1} />
  );

  test("Ensure Avatar exists", () => {
    expect(wrapper.find("img")).toHaveLength(1);
    expect(wrapper.find("img").prop("src")).toBe(
      "http://pbs.twimg.com/profile_images/1437443196821426176/CDJCYki__normal.jpg"
    );
  });

  test("Check username", () => {
    expect(wrapper.text().includes("@wolfiecindy")).toBe(true);

    expect(wrapper.find("a").at(1).prop("href")).toBe(
      "https://twitter.com/wolfiecindy"
    );
  });

  test("Check tweet contents", () => {
    expect(wrapper.find({ "data-testid": "tweet-text" }).text()).toContain(
      "men who love cats"
    );
  });
});
