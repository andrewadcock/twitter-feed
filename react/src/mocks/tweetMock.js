const tweetMock = {
  created_at: "Sat Sep 25 11:16:30 +0000 2021",
  id: 1441723280293912600,
  id_str: "1441723280293912577",
  full_text:
    "when i talk negatively about men, men who love cats aren’t included",
  truncated: false,
  entities: {
    hashtags: [],
    symbols: [],
    user_mentions: [],
    urls: [],
  },
  metadata: {
    result_type: "popular",
    iso_language_code: "en",
  },
  source:
    '<a href="http://twitter.com/download/iphone" rel="nofollow">Twitter for iPhone</a>',
  in_reply_to_status_id: null,
  in_reply_to_status_id_str: null,
  in_reply_to_user_id: null,
  in_reply_to_user_id_str: null,
  in_reply_to_screen_name: null,
  user: {
    id: 3190492830,
    id_str: "3190492830",
    name: "Cindy Kimberly",
    screen_name: "wolfiecindy",
    location: "",
    description: "",
    url: "https://t.co/ZVrfBoPz9w",
    entities: {
      url: {
        urls: [
          {
            url: "https://t.co/ZVrfBoPz9w",
            expanded_url:
              "https://www.thelionsla.com/losangeles/talent/cindy-kimberly/compcard/",
            display_url: "thelionsla.com/losangeles/tal…",
            indices: [0, 23],
          },
        ],
      },
      description: {
        urls: [],
      },
    },
    protected: false,
    followers_count: 677657,
    friends_count: 1933,
    listed_count: 655,
    created_at: "Sun May 10 07:19:50 +0000 2015",
    favourites_count: 10352,
    utc_offset: null,
    time_zone: null,
    geo_enabled: true,
    verified: true,
    statuses_count: 210,
    lang: null,
    contributors_enabled: false,
    is_translator: false,
    is_translation_enabled: false,
    profile_background_color: "C0DEED",
    profile_background_image_url:
      "http://abs.twimg.com/images/themes/theme1/bg.png",
    profile_background_image_url_https:
      "https://abs.twimg.com/images/themes/theme1/bg.png",
    profile_background_tile: false,
    profile_image_url:
      "http://pbs.twimg.com/profile_images/1437443196821426176/CDJCYki__normal.jpg",
    profile_image_url_https:
      "https://pbs.twimg.com/profile_images/1437443196821426176/CDJCYki__normal.jpg",
    profile_banner_url:
      "https://pbs.twimg.com/profile_banners/3190492830/1631548140",
    profile_link_color: "1DA1F2",
    profile_sidebar_border_color: "C0DEED",
    profile_sidebar_fill_color: "DDEEF6",
    profile_text_color: "333333",
    profile_use_background_image: true,
    has_extended_profile: true,
    default_profile: true,
    default_profile_image: false,
    following: null,
    follow_request_sent: null,
    notifications: null,
    translator_type: "none",
    withheld_in_countries: [],
  },
  geo: null,
  coordinates: null,
  place: null,
  contributors: null,
  is_quote_status: false,
  retweet_count: 1716,
  favorite_count: 9046,
  favorited: false,
  retweeted: false,
  lang: "en",
  extended_entities: {},
  setSelectedHashtag: {},
};

export default tweetMock;
