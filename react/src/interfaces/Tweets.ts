/**
 * Interface: Tweets
 */
import ITweet from "./Tweet";

export default interface ITweets {
  search_metadata: any;
  statuses: ITweet[];
}
