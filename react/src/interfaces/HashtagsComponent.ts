/**
 * Interface: HashtagsComponent
 */

export default interface IHashtagsComponent {
  data: any;
  setSelectedHashtag: any;
  selectedHashtag: string;
}
