/**
 * Interface: Tweet
 */

export default interface ITweet {
  created_at?: string;
  id: number;
  id_str?: string;
  full_text: string;
  truncated?: boolean;
  entities?: any;
  metadata?: any;
  source?: string;
  in_reply_to_status_id?: any;
  in_reply_to_status_id_str?: any;
  in_reply_to_user_id?: any;
  in_reply_to_user_id_str?: any;
  in_reply_to_screen_name?: any;
  user: any;
  geo?: any;
  coordinates?: any;
  place?: any;
  contributors?: any;
  is_quote_status?: boolean;
  retweet_count?: number;
  favorite_count?: number;
  favorited?: boolean;
  retweeted?: boolean;
  possibly_sensitive?: boolean;
  lang?: string;
  extended_entities: any;
  setSelectedHashtag: any;
}
