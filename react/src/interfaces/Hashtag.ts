/**
 * Interface: Hashtag
 */

export default interface IHashtag {
  text: string;
  statuses: number[];
}
