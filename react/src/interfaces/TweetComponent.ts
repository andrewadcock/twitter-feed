/**
 * Interface: TweetComponent
 */
import ITweet from "./Tweet";

export default interface ITweetComponent {
  data: ITweet;
  key: number;
  setSelectedHashtag?: any;
  selectedHashtag: string;
}
