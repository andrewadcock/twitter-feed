import React from "react";
import { shallow } from "enzyme";
import App from "./App";
import Tweets from "./components/tweets/Tweets";

describe("Renders participantsInfo component", () => {
  let wrapper = shallow(<App />);

  test("Check that page title shows", () => {
    expect(wrapper.containsMatchingElement(<Tweets />)).toEqual(true);
  });
});
